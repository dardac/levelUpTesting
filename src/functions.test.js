const functions = require ('./functions');

beforeEach (() => initDatabase());
afterEach(() => closeDatabase());

// beforeAll (() => initDatabase());
// afterAll(() => closeDatabase());

const initDatabase = () => console.log('Database Initialized...');
const closeDatabase = () => console.log('Database Closed...');
const nameCheck = () => console.log('Checking Name...');

describe("Checking Names", () => {
  beforeEach(() => nameCheck());

  it("User is Jeff", () => {
    const user = 'Jeff';
    expect(user).toBe('Jeff');
  });
    it("User is Karen", () => {
      const user = 'Karen';
      expect(user).toBe('Karen');
    });
});

//toBeNull
describe("Is null", () => {
  it("should be null", () => {
        expect(functions.isNull()).toBeNull();
  });
});

//toBeFalsy
describe("checkValue", () => {
  it("Should be falsy", () => {
    expect(functions.checkValue(undefined)).toBeFalsy();
    expect(functions.checkValue(null)).toBeFalsy();
    expect(functions.checkValue(0)).toBeFalsy();
    expect(functions.checkValue('')).toBeFalsy();
  });
});

describe("User", () => {
  it("User should be Brad Traversy object", () => {
    expect(functions.createUser()).toEqual({
      firstName: 'Brad',
      lastName: 'Traversy'
    });
  });
});

describe("Less than and greater than", () => {
  it("Should be under 1600", () => {
    const load1 = 800;
    const load2 = 700;
    expect(load1 + load2).toBeLessThan(1600);
  });
});

describe("Working with async data", () => {
  //Promise
/*  it("User fetched name should be Leann Graham", () => {
    expect.assertions(1);
    return functions.fetchUser()
      .then(data => {
        expect(data.name).toEqual('Leanne Graham');
      })
  });*/

  // Async Await
  it("Using async", async () => {
    expect.assertions(1);
    const data = await functions.fetchUser();
    expect(data.name).toEqual('Leanne Graham');
  });

});
