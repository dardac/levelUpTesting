const axios = require('axios');

const functions = {

  isNull : () => null,

  checkValue : x => x,

  createUser : () => {
    const user = { firstName : 'Brad'}
    user['lastName'] = 'Traversy';

    return user;
  },

  fetchUser : () => axios
    .get ('https://jsonplaceholder.typicode.com/users/1')
    .then (res => res.data)
    .catch (err => 'error')


}

module.exports = functions;
