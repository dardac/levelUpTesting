let {hello, add, removeSNames} = require('./App');

describe("hello", () => {
  it("should output hello", () => {
    expect(hello()).toBe('Hello');
  });
});

describe("add", () => {
  it("should add two numbers", () => {
    expect(add(1,2)).toBe(3);
    expect(add(2,2)).toBe(4);
    expect(add(14,-5)).toBe(9);
    expect(add(-2,2)).toBe(0);
  });

  it("should not add strings", () => {
    expect(add(2,'2')).toBe(null);
  });

  it("should not add objects", () => {
    expect(add(2,{})).toBe(null);
  });

  it("should not add array", () => {
    expect(add(2,[])).toBe(null);
  });
});

describe("removeSNames", () => {
  it("should remove all s names", () => {
    const names = ['Scott', 'Courtney'];
    expect(removeSNames(names)).not.toContain('Scott');
  });
  it("should not remove other names", () => {
    const names = ['Scott', 'Courtney', 'Wess'];
    expect(removeSNames(names)).toContain('Courtney');
    expect(removeSNames(names)).toContain('Wess');
  });
  test("should account for case", () => {
    const names = ['Scott', 'Courtney','Wess','scott'];
    expect(removeSNames(names)).not.toContain('scott');
    expect(removeSNames(names)).not.toContain('Scott');
  });
});
