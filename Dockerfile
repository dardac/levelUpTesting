FROM node:7.8-slim

# App workdir
WORKDIR /app

# Install dependecies
RUN npm install -g jest --coverage
RUN npm --allow-root install
RUN npm test

# Build app source code
COPY . ./
